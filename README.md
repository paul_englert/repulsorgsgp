# Repulsor GSGP #

Implementation of GSGP using the concept of semantic repulsors. 

### What is this repository for? ###

* based on [GSGP](http://gsgp.sourceforge.net/)
* modified using NSGA-II and a dynamic number of repulsors, to which the distance has to be maximized.

### Execution/Compilation Notes ###

* use g++ -std=c++11 (needed for tuple type)